import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AboutMeComponent } from './core/aboutme/aboutme.component';

// import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { HomeComponent } from './core/home/home.component';
import { RecipeListHealthyComponent } from './recipes/recipe-list-healthy/recipe-list-healthy.component';
import { RecipeListIndianComponent } from './recipes/recipe-list-Indian/recipe-list-Indian.component';
import { RecipeListMexicanComponent } from './recipes/recipe-list-mexican/recipe-list-mexican.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'recipes', loadChildren: () => import('./recipes/recipes.module').then(m => m.RecipesModule)},
   { path: 'aboutme', component: AboutMeComponent },
   { path: 'recipes-Indian', component: RecipeListIndianComponent },
   { path: 'recipes-Mexican', component: RecipeListMexicanComponent },
   { path: 'recipes-Healthy', component: RecipeListHealthyComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules, relativeLinkResolution: 'legacy' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
