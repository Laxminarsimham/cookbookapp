import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecipeEditComponent } from './recipe-edit/recipe-edit.component';
import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';
import { RecipeStartComponent } from './recipe-start/recipe-start.component';
import { RecipesComponent } from './recipes.component';
import { RecipeListIndianComponent } from './recipe-list-Indian/recipe-list-Indian.component';
import { RecipeListMexicanComponent } from './recipe-list-mexican/recipe-list-mexican.component';
import { RecipeListHealthyComponent } from './recipe-list-healthy/recipe-list-healthy.component';

const recipesRoutes: Routes = [
  { path: '', component: RecipesComponent, children: [
    { path: '', component: RecipeStartComponent },
    { path: 'new', component: RecipeEditComponent },
    { path: ':id', component: RecipeDetailComponent },
    { path: ':id/edit', component: RecipeEditComponent },
    { path: 'recipes-Indian', component: RecipeListIndianComponent },
    { path: 'recipes-Mexican', component: RecipeListMexicanComponent },
    { path: 'recipes-Healthy', component: RecipeListHealthyComponent },
    
  ] },
];

@NgModule({
  imports: [
    RouterModule.forChild(recipesRoutes)
  ],
  exports: [RouterModule],
  providers: [
    
  ]
})
export class RecipesRoutingModule {}
